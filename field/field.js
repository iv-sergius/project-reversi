    // РАЗМЕРЫ ПОЛЯ
CELL_SIZE = 40;
DISK_SIZE = 15;
FIELD_SIZE = 8;
LINE_WIDTH = 3;
BG_COLOR = "#AAA";
LINE_COLOR = "black"

_drawDisk = function (cell, color)
{
  this.ctx.fillStyle = color; 
	this.ctx.beginPath();
  this.ctx.arc((cell.x + 0.5) * this.cellSize, (cell.y + 0.5) * this.cellSize, this.diskSize, 0, Math.PI*2, true); // Disk
  this.ctx.fill();
}

_drawBoard = function ()
{
    for (var i = 0;  i < this.fieldSize; i++)
    {
        for (var j = 0;  j < this.fieldSize; j++)
        {
			var cell = new Cell(i, j);
            if (!this.isCellEmpty(cell)) 
            {
                this.drawDisk(cell, this.getDiskColor(cell));            
            } 
        }
    }
}

_smallDot = function (ctx, x, y)
{
	ctx.beginPath();
    ctx.arc(x * CELL_SIZE, y * CELL_SIZE, LINE_WIDTH, 0, Math.PI*2, true); // Disk
    ctx.stroke();
}

_newField = function()
{
	this.ctx.fillRect(0, 0, this.size, this.size);
	for (var h = 0; h <= this.size; h += this.cellSize) //horizontal
	{
		this.ctx.beginPath();
		this.ctx.moveTo(h, 0);
		this.ctx.lineTo(h, this.size);
		this.ctx.stroke();
	};
	for (var v = 0; v <= this.size; v += this.cellSize) //horizontal
	{
		this.ctx.beginPath();
		this.ctx.moveTo(0, v);
		this.ctx.lineTo(this.size, v);
		this.ctx.stroke();
	}
	_smallDot(this.ctx, 2, 2);
	_smallDot(this.ctx, 2, 6);
	_smallDot(this.ctx, 6, 2);
	_smallDot(this.ctx, 6, 6);
}

function Field(elementId)
{
	var field = document.getElementById(elementId);
/*	if (field == null)
	{
		this = null;
	}
	else
*/
		this.id = elementId;
		this.element = field;
		this.cellSize = CELL_SIZE;
		this.diskSize = DISK_SIZE;
		this.fieldSize = FIELD_SIZE;
		this.size = this.cellSize * this.fieldSize;
		this.lineWidth = LINE_WIDTH;
		this.bgColor = BG_COLOR;
		this.lineColor = LINE_COLOR;
		this.playerColor = ["white", "black"];
		this.ctx = field.getContext("2d");
		this.board = new Board(this.fieldSize);

		field.width = this.cellSize * this.fieldSize + this.lineWidth;
		field.height = this.cellSize * this.fieldSize + this.lineWidth;

		this.ctx.translate(this.lineWidth/2, this.lineWidth/2);
		this.ctx.fillStyle = this.bgColor;
		this.ctx.lineWidth = this.lineWidth;
		this.newField = _newField;
		this.drawDisk = _drawDisk; 
		this.drawBoard = _drawBoard;
		this.getDiskColor = function(cell)
		{
		    return this.playerColor[this.board.getDiskOwner(cell)];
		}
		this.isCellEmpty = function (cell)
		{
		    return this.board.isCellEmpty(cell);
		}
/*    this.element.onclick = function(event)
    {
//        console.log(event.clientX, event.clientY);
        this.board.playerTurn(event.clientX / this.cellSize | 0, event.clientX / this.cellSize | 0);
    }*/
}

function start()
{
    var field = new Field("field");
    field.newField();
    field.drawBoard();
  	console.log(field);
	var cell = new Cell(2, 3);
    console.log(field.board.canPlayerTurn(cell));
    console.log(cell);
    field.board.playerTurn(cell);
    field.drawBoard();
    cell.set(2,2);
    field.board.playerTurn(cell);
    field.drawBoard();
    field.element.onclick = function (event)
    {
        console.log(event.clientX / field.cellSize | 0, event.clientY / field.cellSize | 0);
        cell.set(event.clientX / field.cellSize | 0, event.clientY / field.cellSize | 0);
        console.log(cell);
        field.board.playerTurn(cell);
    }
 /* cell.set(2,4);
    console.log(field.board.playerTurn(cell));
    field.drawBoard();
  
    cell.set(2,4);
    console.log(field.board.canPlayerTurn(cell));
    console.log(field.board.getDiskOwner(cell));
    console.log(field.board.isAnotherOwnerInDirection(cell, 2));
*/
}

