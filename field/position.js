DIRECTOINS = [{x: 1, y:0}, {x: 1, y: 1}, {x:0, y: 1}, {x:-1, y: 1}, 
              {x:-1, y:0}, {x:-1, y:-1}, {x:0, y:-1}, {x: 1, y:-1}];

function Cell(x, y) 
{
    this.x = x;
    this.y = y;
    this.set = function(x, y)
    {
        this.x = x;
        this.y = y;
    }
}

function Board(dimention) 
{
    this.dim = dimention;
    this.map = [];
// inition
    for (var i = 0; i < this.dim; i++)
    { 
       this.map.push([]);
       for (var j = 0; j < this.dim; j++)
       {
          this.map[i].push(-1);
       }
    };
    this.map[this.dim/2    ][this.dim/2    ] = 1;
    this.map[this.dim/2 - 1][this.dim/2    ] = 0;
    this.map[this.dim/2    ][this.dim/2 - 1] = 0;
    this.map[this.dim/2 - 1][this.dim/2 - 1] = 1;
    this.move = 0;
    
    this.isCellOnBoard = function(cell)
    {
        return (cell.x >=0 && cell.x < this.dim && cell.y >=0 && cell.y < this.dim)
    }
    this.isCellEmpty = function (cell) 
    {
      if (this.isCellOnBoard(cell))
      {
        return (this.map[cell.x][cell.y] < 0);
      }
      else 
      {
        return null;
      }  
    };
    this.getDiskOwner = function (cell)
    {
        return this.map[cell.x][cell.y];
    }
    this.isAnotherOwnerInDirection = function(cell, direction) 
    {
        var diskOwner = this.getDiskOwner(cell);
        moveCellInDirection(cell, direction);
        while (this.isCellOnBoard(cell))
        {
            if (this.getDiskOwner(cell) + diskOwner == 1) 
            {
                return true;
            };
            moveCellInDirection(cell, direction);
        }
        return false;
    }
    this.changeOwnerInDirection = function(cell, direction) 
    {
        var diskOwner = this.getDiskOwner(cell);
        var curCell = new Cell(cell.x, cell.y);
        moveCellInDirection(curCell, direction);
        while (this.getDiskOwner(curCell) + diskOwner == 1)
        {
            this.map[curCell.x][curCell.y];
            moveCellInDirection(curCell, direction);
        }
        return false;
    }
    this.canPlayerTurn = function (cell)  
    {
        var newDiskOwner = this.move;
        var curCell = new Cell(cell.x, cell.y);
        for (var i = 0; i < DIRECTOINS.length; i++)
        {
            curCell.set(cell.x, cell.y);
            moveCellInDirection(curCell, i);
            if (this.isCellOnBoard(curCell) && (this.getDiskOwner(curCell) + newDiskOwner == 1) )
            {
                if (this.isAnotherOwnerInDirection(curCell, i))
                {
                    return true;
                }
            }
            
        }
        return false;
    };
    this.playerTurn = function (cell)  
    {
        if (this.canPlayerTurn(cell))
        {
            this.map[cell.x][cell.y] = this.move;
            var newDiskOwner = this.move;
            var curCell = new Cell(cell.x, cell.y);
            for (var i = 0; i < DIRECTOINS.length; i++)
            {
                curCell.set(cell.x, cell.y);
                moveCellInDirection(curCell, i);
                // Если соседняя клетка другого цвета
                if (this.isCellOnBoard(curCell) && (this.getDiskOwner(curCell) + newDiskOwner == 1))
                {
                    if (this.isAnotherOwnerInDirection(curCell, i))
                    {
                        curCell.set(cell.x, cell.y);
                        moveCellInDirection(curCell, i);
                        while (this.getDiskOwner(curCell) != newDiskOwner)
                        {
                            this.map[curCell.x][curCell.y] = this.move;
                            moveCellInDirection(curCell, i);
                        }
                    }
                }
            }
            this.move = 1 - this.move; 
        }
    };

}

moveCellInDirection = function(cell, direction) 
{
    if ((direction >=0) && (direction < DIRECTOINS.length)) {
//console.log(cell);
        cell.x += DIRECTOINS[direction].x;
        cell.y += DIRECTOINS[direction].y;
//console.log(cell);
    }
};

listPosibleMoves = function () 
{
    
}
