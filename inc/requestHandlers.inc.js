var querystring = require("querystring"),
    fs = require("fs"),
    formidable = require("formidable"),
    sql = require("./sql.inc");
    
var handle = {}
handle["/"] = start;
handle["/start"] = start;
handle["/login"] = login;
handle["/show"] = show;    
handle["/css/style.css"] = css;

function sendFromFileToResponse(file, response)
{
    fs.readFile(file, "utf8", function (err, body) {
        if (err) {
            console.log(err);
            response.end();
        }
        else
        {
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(body);
            response.end();
        }
    });
}

function start(response) {
    console.log("Request handler 'start' was called.");
    sendFromFileToResponse("./template/login.tpl", response);
}

function login(response, request) {
    console.log("Request handler 'login' was called.");
    var form = new formidable.IncomingForm();
    console.log("about to parse");
    form.parse(request, function(error, fields, files) {
        console.log("parsing done");
        console.log(fields.login_name, fields.password);
    
    console.log(sql.isLoginFree(sql.connection, fields.login_name));
    
/*        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("received image:<br/>");
        response.write("<img src='/show' />");
        response.end();*/
    });
}

function show(response) {
  console.log("Request handler 'show' was called.");
  fs.readFile("/tmp/test.png", "binary", function(error, file) {
    if(error) {
      response.writeHead(500, {"Content-Type": "text/plain"});
      response.write(error + "\n");
      response.end();
    } else {
      response.writeHead(200, {"Content-Type": "image/png"});
      response.write(file, "binary");
      response.end();
    }
  });
}

function css(response) {
    sendFromFileToResponse("./css/style.css", response);
}

exports.start = start;
exports.login = login;
exports.show = show;
exports.css = css;
exports.handle = handle;

