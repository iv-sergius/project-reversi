var server = require("./inc/server.inc"),
    router = require("./inc/router.inc"),
    requestHandlers = require("./inc/requestHandlers.inc");

server.start(router.route, requestHandlers.handle);
